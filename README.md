<a name="readme-top"></a>
## Built With

### Tech
1. [bunjs](https://bun.sh/)
2. [expressjs](http://expressjs.com/)

### DB
1. [PostgreSQL](https://www.postgresql.org/)
2. [Redis](https://redis.io/)

### More
1. [Docker](https://www.docker.com/)
2. [Drizzle ORM](https://orm.drizzle.team/)
3. [Bcrypt](https://github.com/dcodeIO/bcrypt.js)
4. [Jsonwebtoken](https://github.com/auth0/node-jsonwebtoken)

<p align="right">(<a href="#readme-top">back to top</a>)</p>