import express, { Request, Response, NextFunction } from "express";
import cookieParse from 'cookie-parser';
import cors from 'cors'
import doMigrate from "./src/db/migrate";
import userRouter from './src/routes/user';
import authRouter from './src/routes/auth'
import { connectRedis } from "./src/db/redis";

const app = express();
const port = 8080;

app.use(express.json({ limit: '10kb' }));
app.use(cookieParse());

app.use(cors({
	origin: 'http://localhost:3000',
	credentials: true,
	methods: 'GET,HEAD,PUT,PATCH,POST,DELETE'
}))

app.use('/api', authRouter)
app.use('/api', userRouter)

app.get('/healthChecker', (req: Request, res: Response, next: NextFunction) => {
	res.status(200).json({
		status: 'success',
		message: 'Welcome bro',
	});
});

app.all('*', (req: Request, res: Response, next: NextFunction) => {
	const err = new Error(`Route ${req.originalUrl} not found`) as any;
	err.statusCode = 404;
	next(err);
});

app.use((err: any, req: Request, res: Response, next: NextFunction) => {
	err.status = err.status || 'error';
	err.statusCode = err.statusCode || 500;

	res.status(err.statusCode).json({
		status: err.status,
		message: err.message,
	});
});

app.listen(port, async () => {
	console.log(`Listening on port ${port}...`);
	await doMigrate();
	await connectRedis();
});
