import { CookieOptions, NextFunction, Request, Response } from "express"
import bcrypt from 'bcryptjs'
import { usersSchema } from "../db/schema/users"
import { db } from "../db/db"
import { loginUserInput, registerUserInput } from "../validation/auth"
import { eq } from "drizzle-orm"
import AppError from "../utils/appError"
import { signJwt, verifyJwt } from "../utils/jwt"
import { redisClient } from "../db/redis"

// Cookie options
const accessTokenCookieOptions: CookieOptions = {
    expires: new Date(
        Date.now() + 15 * 60 * 1000
    ),
    maxAge: 15 * 60 * 1000,
    httpOnly: true,
    sameSite: 'strict',
};

const refreshTokenCookieOptions: CookieOptions = {
    expires: new Date(
        Date.now() + 60 * 60 * 1000
    ),
    maxAge: 60 * 60 * 1000,
    httpOnly: true,
    sameSite: 'strict',
};

// Only set secure to true in production
if (process.env.NODE_ENV === 'production') {
    accessTokenCookieOptions.secure = true;
    refreshTokenCookieOptions.secure = true;
}

export const userRegister = async (req: Request<{}, {}, registerUserInput>, res: Response) => {
    try {
        const hashedPassword = await bcrypt.hash(req.body.password, 12);

        await db.insert(usersSchema).values({
            email: req.body.email,
            firstName: req.body.firstName,
            lastName: req.body.lastName,
            password: hashedPassword,
            role: "user"
        });

        res.status(201).json({
            message: "success"
        });
    } catch (error) {
        return res.status(409).json({
            code: error.code,
            message: error.message
        });
    }
}

export const userLogin = async (req: Request<{}, {}, loginUserInput>, res: Response, next: NextFunction) => {
    try {
        const user = await db.select({
            id: usersSchema.id,
            firstName: usersSchema.firstName,
            lastName: usersSchema.lastName,
            email: usersSchema.email,
            role: usersSchema.role,
            password: usersSchema.password
        }).from(usersSchema).where(eq(usersSchema.email, req.body.email));
        const hashedPassword = user.map(data => data.password)
        if (!user || !(await bcrypt.compare(req.body.password, hashedPassword[0]))) {
            return next(new AppError("invalid email or password", 401));
        }

        // create access token
        const userId = user.map(data => data.id);
        const accessToken = signJwt({ sub: userId[0] }, `${process.env.ACCESS_TOKEN_PRIVATE_KEY}`, {
            expiresIn: '15m'
        });

        const refreshToken = signJwt({ sub: userId[0] }, `${process.env.REFRESH_TOKEN_PRIVATE_KEY}`, {
            expiresIn: '60m'
        });

        redisClient.set(`${userId[0]}`, JSON.stringify(user), {
            EX: 60 * 60
        });

        res.cookie('refreshToken', refreshToken, refreshTokenCookieOptions);

        return res.status(200).json({
            status: 'success',
            accessToken
        });
    } catch (error) {
        next(error);
    }
}


export const refreshTokenHandler = async (req: Request, res: Response, next: NextFunction) => {
    try {
        const refreshToken = req.cookies.refreshToken;

        const decoded = verifyJwt<{ sub: string }>(refreshToken, `${process.env.REFRESH_TOKEN_PUBLIC_KEY}`);
        if (!decoded) {
            return next(new AppError('could not refresh access token', 403))
        }

        const session = await redisClient.get(`${decoded.sub}`);
        if (!session) {
            return next(new AppError('could not refresh access token', 403))
        }

        const user = await db.select({
            id: usersSchema.id,
            firstName: usersSchema.firstName,
            lastName: usersSchema.lastName,
            email: usersSchema.email,
            role: usersSchema.role
        }).from(usersSchema).where(eq(usersSchema.id, JSON.parse(session)[0].id));
        if (!user) {
            return next(new AppError('could not refresh access token', 403))
        }

        const userId = user.map(data => data.id);
        const accessToken = signJwt({ sub: userId[0] }, `${process.env.ACCESS_TOKEN_PRIVATE_KEY}`, {
            expiresIn: '15m'
        });

        return res.status(200).json({
            status: "success",
            accessToken
        })
    } catch (error) {
        next(error)
    }
}