import { Response, Request, NextFunction } from "express";
import { db } from "../db/db";
import { usersSchema } from "../db/schema/users";
import { redisClient } from "../db/redis";
import AppError from "../utils/appError";
import { verifyJwt } from "../utils/jwt";

export const getMe = async (req: Request, res: Response, next: NextFunction) => {
    try {
        const me = res.locals.user;
        return res.status(200).json({
            status: "success",
            data: me
        })
    } catch (error) {
        next(error)
    }
}

export const getAllUsers = async (req: Request, res: Response, next: NextFunction) => {
    try {
        const users = await db.select({
            firstName: usersSchema.firstName,
            lastName: usersSchema.lastName,
            email: usersSchema.email
        }).from(usersSchema);

        return res.status(200).json({
            data: users,
            result: users.length
        })
    } catch (error) {
        next(error)
    }
}

export const logOut = async (req: Request, res: Response, next: NextFunction) => {
    try {
        let accessToken;
        if (req.headers.authorization && req.headers.authorization.startsWith('Bearer')) {
            accessToken = req.headers.authorization.split(' ')[1]
        }
        if (!accessToken) {
            return next(new AppError('kamu tidak login', 401));
        }

        const decoded = verifyJwt<{ sub: string }>(accessToken, `${process.env.ACCESS_TOKEN_PUBLIC_KEY}`);
        if (!decoded) {
            return next(new AppError(`Invalid token or user doesn't exist`, 401))
        }
        
        await redisClient.del(`${decoded.sub}`);
        res.locals.user = null;

        return res.status(200).json({
            status: 'success logout',
        });
    } catch (error) {
        console.log(error)
        next(error)
    }
}