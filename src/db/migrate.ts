import { Client } from "pg";
import { drizzle } from "drizzle-orm/node-postgres";
import { migrate } from "drizzle-orm/node-postgres/migrator";

const doMigrate = async () => {
	try {
		console.log("start migration")
		const client = new Client({
			connectionString: process.env.POSTGRES_URL,
		});

		await client.connect();
		const db = drizzle(client);

		await migrate(db, { migrationsFolder: ".drizzle/migrations" });
		console.log("migration done");
	} catch (error) {
		console.log("migration error: ", error)
		process.exit(0);
	}
}
export default doMigrate