import { createClient } from 'redis';

export const redisClient = createClient({
    url: process.env.REDIS_URL
});

export const connectRedis = async () => {
    try {
        await redisClient.connect();
        console.log("redis connect");
    } catch (error) {
        console.log("redis error: ", error.message);
        setTimeout(() => {
            connectRedis
        }, 5000);
    }
};

redisClient.on('error', error => console.log(error));