import { boolean, pgEnum, pgTable, serial, text, timestamp, uniqueIndex, varchar } from 'drizzle-orm/pg-core';

export const roleEnum = pgEnum('role', ['admin', 'user']);

export const usersSchema = pgTable('users', {
    id: serial('id').primaryKey(),
    email: varchar('email', { length: 50 }).notNull(),
    firstName: varchar('firstName', { length: 50 }).notNull(),
    lastName: varchar('lastName', { length: 50 }).notNull(),
    photo: varchar('photo', {length: 30}).default('default.jpg'),
    password: varchar('password', { length: 255 }).notNull(),
    verificationCode: text('verificationCode').unique(),
    passwordResetCode: text('passwordResetCode').unique(),
    verified: boolean('verified').default(false),
    role: roleEnum('role').notNull().default('user'),
    createdAt: timestamp('created_at').defaultNow().notNull(),
    updatedAt: timestamp('updated_at').defaultNow().notNull(),
}, (users) => {
    return {
        emailIndex: uniqueIndex('email_idx').on(users.email),
    }
});