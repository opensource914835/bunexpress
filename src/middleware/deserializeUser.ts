import { NextFunction, Request, Response } from "express"
import AppError from "../utils/appError";
import { verifyJwt } from "../utils/jwt";
import { redisClient } from "../db/redis";
import { db } from "../db/db";
import { usersSchema } from "../db/schema/users";
import { eq } from "drizzle-orm";
export const deserializeUser = async (req: Request, res: Response, next: NextFunction) => {
    try {
        let accessToken;
        if (req.headers.authorization && req.headers.authorization.startsWith('Bearer')) {
            accessToken = req.headers.authorization.split(' ')[1]
        }
        if (!accessToken) {
            return next(new AppError('kamu tidak login', 401));
        }

        const decoded = verifyJwt<{ sub: string }>(accessToken, `${process.env.ACCESS_TOKEN_PUBLIC_KEY}`);
        if (!decoded) {
            return next(new AppError(`Invalid token or user doesn't exist`, 401))
        }

        const session = await redisClient.get(`${decoded.sub}`);
        if (!session) {
            return next(new AppError(`User session has expired`, 401))
        }

        const user = await db.select({
            firstName: usersSchema.firstName,
            lastName: usersSchema.lastName,
            email: usersSchema.email,
            role: usersSchema.role
        }).from(usersSchema).where(eq(usersSchema.email, JSON.parse(session)[0].email));
        if (!user[0]) {
            return next(new AppError('user tidak ada', 401))
        }

        res.locals.user = user[0]
        next()
    } catch (error) {
        next(error)
    }
}