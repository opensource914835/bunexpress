import { Request, Response, NextFunction } from "express";
import AppError from "../utils/appError";

export const requireUser = async (req: Request, res: Response, next: NextFunction) => {
    try {
        const user = res.locals.user
        if(!user){
            return next(new AppError('invalid token or session has expired', 401))
        }

        next()
    } catch (error) {
        next(error)
    }
}