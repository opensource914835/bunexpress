import express from 'express';
import { validate } from '../middleware/validate';
import { loginUserSchema, userRegisterSchema } from '../validation/auth';
import { refreshTokenHandler, userLogin, userRegister } from '../controller/auth';

const router = express.Router();

router.post("/register", validate(userRegisterSchema), userRegister);
router.post("/login", validate(loginUserSchema), userLogin);
router.get('/refresh', refreshTokenHandler);

export default router