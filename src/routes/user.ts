import express from 'express';
import { deserializeUser } from '../middleware/deserializeUser';
import { requireUser } from '../middleware/requireUser';
import { restrictTo } from '../middleware/restrictTo';
import { getAllUsers, getMe, logOut } from '../controller/user';

const router = express.Router()

router.use(deserializeUser, requireUser);

router.get("/", restrictTo('admin'), getAllUsers)
router.get("/me", getMe)
router.post("/logout", logOut)

export default router