import { object, string, TypeOf, z } from 'zod';

export const userRegisterSchema = object({
    body: object({
        firstName: string({ required_error: "First name is required" }),
        lastName: string({ required_error: "Last name is required" }),
        email: string({ required_error: "Email is required" }).email('Ivalid email'),
        password: string({ required_error: "Password is required" }).min(4, "minimal 4").max(32, "max 32"),
        passwordConfirm: string({ required_error: "Please confirm your password" }),
    }).refine((data) => data.password === data.passwordConfirm, {
        path: ['passwordConfirm'],
        message: 'Passwords do not match'
    }),
});

export const loginUserSchema = object({
    body: object({
        email: string({ required_error: 'Email is required' }).email('Invalid email or password'),
        password: string({ required_error: 'Password is required' }).min(4,'Invalid email or password'),
    }),
});

export type registerUserInput = TypeOf<typeof userRegisterSchema>['body'];
export type loginUserInput = TypeOf<typeof loginUserSchema>['body'];